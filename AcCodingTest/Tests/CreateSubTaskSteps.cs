﻿using System;
using TechTalk.SpecFlow;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using NUnit.Framework;
using AcCodingTest.PageModels;
using System.Text.RegularExpressions;

namespace AcCodingTest
{
    [Binding]
    public class CreateSubTaskSteps
    {

        IWebDriver driver;
        HomePage homePage;
        LoginPage loginPage;
        TasksPage tasksPage;
        SubTasksPage subtasksPage;
        int subTasksCreated = 0;

        [BeforeScenario]
        public void SetUp()
        {
            driver = new ChromeDriver();

            homePage = new HomePage(driver);
            loginPage = new LoginPage(driver);
            tasksPage = new TasksPage(driver);
            subtasksPage = new SubTasksPage(driver);
        }

        [AfterScenario]
        public void TearDown()
        {
            driver.Close();
            driver.Quit();
        }
        
        [When(@"I click on the \[Manage Subtasks] button of a task")]
        public void WhenIClickOnTheManageSubtasksButtonOfATask()
        {
            tasksPage.ManageSubTasksButton().Click();
        }
        
        [Then(@"a modal dialog is shown")]
        public void ThenAModalDialogIsShown()
        {
            Assert.IsTrue(subtasksPage.ModalDialogSubTaskCreation().Displayed);
        }
        
        [Then(@"the task's ID is shown, a read-only field")]
        public void ThenTheTaskSIDIsShownARead_OnlyField()
        {
            string regexPattern = "[Editing Task ]\\d{1,}";
            Regex regex = new Regex(regexPattern, RegexOptions.IgnoreCase);

            Assert.IsTrue(regex.IsMatch(subtasksPage.TaskID().Text));
        }

        [Then(@"the tasks description matches the task on which I clicked the \[Manage Subtasks] button, '(.*)'")]
        public void ThenTheTasksDescriptionMatchesTheTaskOnWhichIClickedTheManageSubtasksButton(string taskDescription)
        {
            Assert.AreEqual(taskDescription, subtasksPage.TasksDescription().GetAttribute("value"));
        }

        [Given(@"I am on the subtask creation form")]
        public void GivenIAmOnTheSubtaskCreationForm()
        {
            WhenIClickOnTheManageSubtasksButtonOfATask();

            subTasksCreated = subtasksPage.CountSubTasksCreated();
        }

        [When(@"I fill out the subtasks description with '(.*)'")]
        public void WhenIFillOutTheSubtasksDescriptionWith(string subtaskDescription)
        {
            subtasksPage.SubTaskDescription().SendKeys(subtaskDescription);
        }


        [When(@"I set a due date '(.*)'")]
        public void WhenISetADueDateFormat(string dueDate)
        {
            subtasksPage.SubTaskDueDate().SendKeys(dueDate);
        }

        [When(@"I click on the \[\+Add] button")]
        public void WhenIClickOnTheAddButton()
        {
            subtasksPage.AddSubTaskButton().Click();
        }

        [Then(@"the subtask is appended to the bottom part of the modal dialog")]
        public void ThenTheSubtaskIsAppendedToTheBottomPartOfTheModalDialog()
        {
            subTasksCreated++;

            Assert.AreEqual(subTasksCreated, subtasksPage.CountSubTasksCreated());
        }

        [Then(@"the counter present in the \[Manage Subtasks] button is increased by one")]
        public void ThenTheCounterPresentInTheManageSubtasksButtonIsIncreasedByOne()
        {
            subtasksPage.CloseButton().Click();

            Assert.AreEqual(string.Format("({0}) Manage Subtasks", subTasksCreated), 
                                            tasksPage.ManageSubTasksButton().Text);
        }
        [When(@"I clear the due date field")]
        public void WhenIClearTheDueDateField()
        {
            subtasksPage.SubTaskDueDate().Clear();
        }

        [Then(@"no subtask is created")]
        public void ThenNoSubtaskIsCreated()
        {
            Assert.AreEqual(subTasksCreated, subtasksPage.CountSubTasksCreated());
        }

        [When(@"I clear the description field")]
        public void WhenIClearTheDescriptionField()
        {
            subtasksPage.SubTaskDescription().Clear();
        }
    }
}
