﻿using System;
using TechTalk.SpecFlow;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using NUnit.Framework;
using AcCodingTest.PageModels;

namespace AcCodingTest.Tests
{
    [Binding]
    public class CreateTaskSteps
    {

        IWebDriver driver;
        HomePage homePage;
        LoginPage loginPage;
        TasksPage tasksPage;
        int tasksCreated = 0;

        [BeforeScenario]
        public void SetUp()
        {
            driver = new ChromeDriver();

            homePage = new HomePage(driver);
            loginPage = new LoginPage(driver);
            tasksPage = new TasksPage(driver);
        }

        [AfterScenario]
        public void TearDown()
        {
            driver.Close();
            driver.Quit();
        }

        [Given(@"I am on the home page")]
        public void GivenIAmOnTheHomePage()
        {
            homePage.GoToHomePage("http://qa-test.avenuecode.com/");
        }

        [When(@"I successfully log in")]
        public void WhenISuccessfullyLogIn()
        {
            homePage.SignIn().Click();

            loginPage.email().SendKeys("johnffreire@gmail.com");
            loginPage.password().SendKeys("acCodingTest");
            loginPage.signInButton().Click();
        }

        [Then(@"I see the '(.*)' link on the loaded page")]
        public void ThenISeeTheLinkOnTheLoadedPage(string label)
        {
            Assert.AreEqual(homePage.MyTasksButton().Text, label);
        }

        [When(@"I click on My tasks link")]
        public void WhenIClickOnMyTasksLink()
        {
            homePage.MyTasksButton().Click();

            tasksCreated = tasksPage.CountTasksCreated();
        }

        [Then(@"I am sent to the task creation page")]
        public void ThenIAmSentToTheTaskCreationPage()
        {
            Assert.AreEqual(driver.Url, "http://qa-test.avenuecode.com/tasks");
        }

        [Then(@"I see a message that says '(.*)' on the top part of the screen")]
        public void ThenISeeAMessageThatSaysOnTheTopPartOfTheScreen(string welcomeMessage)
        {
            Assert.AreEqual("Hey, Johnatha Felix Freire, this is your todo list for today",
                tasksPage.MessageTopPart().Text);
        }

        [Given(@"I am on the task creation page")]
        public void GivenIAmOnTheTaskCreationPage()
        {
            GivenIAmOnTheHomePage();
            WhenISuccessfullyLogIn();
            WhenIClickOnMyTasksLink();
        }

        [When(@"I type a task description '(.*)'")]
        public void WhenITypeATaskDescription(string taskDescription)
        {
            tasksPage.TaskDescription().SendKeys(taskDescription);
        }

        [When(@"I hit \[Enter]")]
        public void WhenIHitEnter()
        {
            tasksPage.TaskDescription().SendKeys(Keys.Enter);
        }

        [Then(@"the task I recently created is appended to the list of tasks")]
        public void ThenTheTaskIRecentlyCreatedIsAppendedToTheListOfTasks()
        {
            tasksCreated++;

            Assert.AreEqual(tasksCreated, tasksPage.CountTasksCreated()); 
        }

        [When(@"I click on the \[\+] button")]
        public void WhenIClickOnTheAddButton()
        {
            tasksPage.AddTaskButton().Click();
        }

        [When(@"I type no task description")]
        public void WhenITypeNoTaskDescription()
        {
            tasksPage.TaskDescription().Clear();
        }

        [Then(@"no task is created")]
        public void ThenNoTaskIsCreated()
        {
            Assert.AreEqual(tasksCreated, tasksPage.CountTasksCreated());
        }

    }
}
