﻿Feature: CreateSubTask
	As a ToDo App user
	I should be able to create a subtask
	So I can break down my tasks in smaller pieces

Scenario: Accessing the subtask creation form
	Given I am on the task creation page
	When I click on the [Manage Subtasks] button of a task
	Then a modal dialog is shown
	And the task's ID is shown, a read-only field
	And the tasks description matches the task on which I clicked the [Manage Subtasks] button, 'MY SECOND TASK'

Scenario: Creating a subtask
	Given I am on the subtask creation form
	When I fill out the subtasks description with 'MY FIRST SUBTASK'
	And I set a due date '06/29/2017'
	And I click on the [+Add] button
	Then the subtask is appended to the bottom part of the modal dialog
	And the counter present in the [Manage Subtasks] button is increased by one

Scenario: Creating a subtask without a due date
	Given I am on the subtask creation form
	When I fill out the subtasks description with 'MY SECOND SUBTASK'
	And I clear the due date field
	And I click on the [+Add] button
	Then no subtask is created

Scenario: Creating a subtask without a description
	Given I am on the subtask creation form
	When I set a due date '06/30/2017'
	And I clear the description field
	And I click on the [+Add] button
	Then no subtask is created