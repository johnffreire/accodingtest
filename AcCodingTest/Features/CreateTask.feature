﻿Feature: CreateTask
	As a ToDo App user
	I should be able to create a task
	So I can manage my tasks

Scenario: Seeing 'My Tasks' link
	Given I am on the home page
	When I successfully log in
	Then I see the 'My Tasks' link on the loaded page

Scenario: Going to the task creation page
	Given I am on the home page
	When I successfully log in
	And I click on My tasks link
	Then I am sent to the task creation page
	And I see a message that says 'Hey, Johnatha Felix Freire, this is your todo list for today' on the top part of the screen

Scenario: Creating a task using the [Enter] key
	Given I am on the task creation page	
	When I type a task description 'MY FIRST TASK'
	And I hit [Enter]
	Then the task I recently created is appended to the list of tasks


Scenario: Creating a task using the [+] button
	Given I am on the task creation page	
	When I type a task description 'MY SECOND TASK'
	And I click on the [+] button
	Then the task I recently created is appended to the list of tasks


Scenario: Creating a task with empty description using [+] button
	Given I am on the task creation page	
	When I type no task description
	And I click on the [+] button
	Then no task is created
 

Scenario: Creating a task with empty description using the [Enter] key
	Given I am on the task creation page	
	When I type no task description
	And I hit [Enter]
	Then no task is created


Scenario: Creating a task with short description using the [Enter] key
	Given I am on the task creation page	
	When I type a task description 'MY'
	And I hit [Enter]
	Then no task is created


Scenario: Creating a task with short description using the [+] button
	Given I am on the task creation page	
	When I type a task description 'MY'
	And I click on the [+] button
	Then no task is created

Scenario: Creating a task with long description using the [Enter] key
	Given I am on the task creation page
	When I type a task description 'SBHERQOTMLY1FE7Q8JFAARNM9193Z2VXJ2H0YW0RNOY8TVP7KA7181D78S0X0L19T8ZYAPFOK32J5HGFITMWKPTJN0MDRDCRS2FSXKMNDFWO22AQ2MCCIWLV2EZZH1GF91YDBQQVVD9OL9KD2NWAPNVHR0MFSTK708A2P73QA2K1IV5A8RANKBA222701HDSWUKRRD7RLYI9ZT9XQPQ1769FY65QTO8F8YWPHA6SYVSNU7QB3MI0IH56DG2WUG'
	And I hit [Enter]
	Then no task is created

Scenario: Creating a task with long description using the [+] button
	Given I am on the task creation page
	When I type a task description 'HU6E2QIGTA0L3IP6ZAQ83KLYVEHYUTY2DA6M6USQUY137WZCWWQ56197LGV6FZEJFAVBUTRUHYNUKDW6ZT1BK18CN98TECIJSJKS2IC965AXOWTDV1U6888L7MKR4S021RK9F2PRXPECREVD5WP3U3U8FLPPJWHRDRQYZ5FMKK42O65ZSKSCDSQY36DTS0ABHQZM24YSU9K85VDNLV54UM83IRMGHMX4JMGBWLTXKKVVLFOD0J70VMUJ365RJS'
	And I click on the [+] button
	Then no task is created