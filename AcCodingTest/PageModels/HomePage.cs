﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace AcCodingTest.PageModels
{
    public class HomePage
    {
        private IWebDriver driver = null;

        public HomePage(IWebDriver driver)
        {
            this.driver = driver;
        }

        public IWebElement SignIn()
        {
            return driver.FindElement(By.LinkText("Sign In")); 
        }

        public void GoToHomePage(string url)
        {
            driver.Navigate().GoToUrl(url);
        }

        public IWebElement MyTasksButton()
        {
            return driver.FindElement(By.CssSelector("body > div.container > div.jumbotron.grey-jumbotron > center > a"));
        }
    }
}
