﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace AcCodingTest.PageModels
{
    public class SubTasksPage
    {
        private IWebDriver driver = null;

        public SubTasksPage(IWebDriver driver)
        {
            this.driver = driver;
        }

        public IWebElement ModalDialogSubTaskCreation()
        {
            return driver.FindElement(By.CssSelector("body > div.modal.fade.ng - isolate - scope.in > div"));
        }

        public IWebElement TaskID()
        {
            return driver.FindElement(By.CssSelector("body > div.modal.fade.ng-isolate-scope.in > div > div > div.modal-header.ng-scope > h3"));
        }

        public IWebElement TasksDescription()
        {
            return driver.FindElement(By.Id("edit_task"));
        }
        
        public IWebElement SubTaskDescription()
        {
            return driver.FindElement(By.Id("new_sub_task"));  
        }

        public IWebElement SubTaskDueDate()
        {
            return driver.FindElement(By.Id("dueDate"));
        }

        public IWebElement AddSubTaskButton()
        {
            return driver.FindElement(By.Id("add-subtask"));
        }

        public IWebElement CloseButton()
        {
            return driver.FindElement(By.CssSelector("body > div.modal.fade.ng-isolate-scope.in > div > div > div.modal-footer.ng-scope > button"));
        }

        public IWebElement SubTasksGrid()
        {
            return driver.FindElement(By.CssSelector("body > div.modal.fade.ng-isolate-scope.in > div > div > div.modal-body.ng-scope > div:nth-child(4) > table"));
        }

        public IReadOnlyCollection<IWebElement> GetCreatedSubTasks()
        {
            return SubTasksGrid().FindElements(By.TagName("tr"));
        }

        public int CountSubTasksCreated()
        {
            return GetCreatedSubTasks().Count;
        }
    }
}