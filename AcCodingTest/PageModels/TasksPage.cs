﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace AcCodingTest.PageModels
{
    public class TasksPage
    {
        private IWebDriver driver = null;

        public TasksPage(IWebDriver driver)
        {
            this.driver = driver;
        }

        public IWebElement MessageTopPart()
        {
            return driver.FindElement(By.CssSelector("body > div.container > h1"));
        }

        public IWebElement TaskDescription()
        {
            return driver.FindElement(By.CssSelector("#new_task"));
        }

        public IWebElement AddTaskButton()
        {
            return driver.FindElement(By.CssSelector("body > div.container > div.task_container.ng-scope > div.well > form > div.input-group > span"));
        }

        public IWebElement ManageSubTasksButton()
        {
            return driver.FindElement(By.CssSelector("body > div.container > div.task_container.ng-scope > div.bs-example > div > table > tbody > tr:nth-child(6) > td:nth-child(4) > button"));
        }

        public IWebElement TasksGrid()
        {
            return driver.FindElement(By.CssSelector("body > div.container > div.task_container.ng-scope > div.bs-example > div > table"));
        }

        public IReadOnlyCollection<IWebElement> GetCreatedTasks()
        {
            return TasksGrid().FindElements(By.TagName("tr"));
        }

        public int CountTasksCreated()
        {
            return GetCreatedTasks().Count;
        }

    }
}
