﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace AcCodingTest.PageModels
{
    public class LoginPage
    {
        private IWebDriver driver = null;

        public LoginPage(IWebDriver driver)
        {
            this.driver = driver;
        }

        public IWebElement email()
        {
            return driver.FindElement(By.Id("user_email"));
        }

        public IWebElement password()
        {
            return driver.FindElement(By.Id("user_password"));
        }

        public IWebElement signInButton()
        {
            return driver.FindElement(By.CssSelector("#new_user > input"));
        }
    }
}
